#include "Collider.h"
#include "Tile.h"

Tile::Tile(SDL_Renderer* gRenderer, Sprite* sprite, std::vector<float> pos, std::vector<float> rot)
: GameObject(gRenderer, sprite, NULL, pos, rot)
{
    m_Collider = new Collider(this, m_Sprite->width, m_Sprite->height);
    m_Collider->SetIsStatic(true);
}

Tile::~Tile()
{
    delete m_Collider;
}

void Tile::Update(float deltaTime)
{}

void Tile::Draw()
{
    SDL_Rect dimen = {.x = (int)m_vPosition[0], .y = (int)m_vPosition[1], .w = 96, .h = 96};
    SDL_RenderCopy(m_gRenderer, m_Sprite->m_tTexture, NULL, &dimen);
    if (Constants::DEBUG_COLLIDERS)
        DrawCollider();
}

void Tile::DrawCollider()
{
    if (m_Collider->GetType() == ColliderType::Rectangle)
    {
        SDL_Rect rect = {m_vPosition[0], m_vPosition[1], m_Collider->GetWidth(), m_Collider->GetHeight()};
        SDL_SetRenderDrawColor(m_gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);
        SDL_RenderFillRect(m_gRenderer, &rect);
    }
}
