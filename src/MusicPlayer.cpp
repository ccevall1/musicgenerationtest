#include <iostream>
#include "MusicPlayer.h"
#include "Static.h"

MusicPlayer::MusicPlayer()
: m_fSecondsPerBeat(.3),
  m_fTimeStepBuffer(0),
  m_nNoteBufferIndex(0),
  m_nRhythmBufferIndex(0),
  m_nInjectedNoteBufferIndex(0),
  m_nInjectedRhythmBufferIndex(0),
  m_nInjectedNumBeats(0),
  m_BassNote(NULL)
{
    m_BassNote = new Sound("./audio/A.ogg");
}

MusicPlayer::~MusicPlayer()
{
    delete m_BassNote;
}

void MusicPlayer::Run(float deltaTime)
{
    m_fTimeStepBuffer += deltaTime;
    if (m_fTimeStepBuffer > m_fSecondsPerBeat)
    {
        // Play overlayed melody
        /*if (!m_InjectedNoteBuffer.m_Notes.empty())
        {
            if (m_nInjectedRhythmBufferIndex < m_nInjectedNumBeats)
            {
                if (m_InjectedRhythmBuffer[m_nInjectedRhythmBufferIndex] == 1)
                {
                    for (Sound s : m_InjectedNoteBuffer.m_Notes[m_nInjectedNoteBufferIndex])
                    {
                        s.PlaySound(1);
                    }
                    m_nInjectedNoteBufferIndex = (m_nInjectedNoteBufferIndex + 1) % m_InjectedNoteBuffer.m_nLength;
                }
            }
            else
            {
                m_InjectedNoteBuffer.m_Notes.clear();
                m_InjectedRhythmBuffer.clear();
                m_nInjectedNoteBufferIndex = 0;
                m_nInjectedRhythmBufferIndex = 0;
                //ReleaseBPMLock();
            }
            m_nInjectedRhythmBufferIndex = (m_nInjectedRhythmBufferIndex+1);
        }*/
        // Play bass note
        if (m_BassNote)
            m_BassNote->PlaySound(Constants::TRACK_BASS);

        // Play base melody
        if (m_RhythmBuffer[m_nRhythmBufferIndex] == 1)
        {
            // play default melody
            int i = 0;
            for (Sound s : m_NoteBuffer.m_Notes[m_nNoteBufferIndex])
            {
                // Play chord
                s.PlaySound(i);
                i++;
            }
            // Next note
            m_nNoteBufferIndex = (m_nNoteBufferIndex + 1) % m_NoteBuffer.m_nLength;
        }
        m_fTimeStepBuffer = 0;
        m_nRhythmBufferIndex = (m_nRhythmBufferIndex + 1) % m_nNumBeats;
    }
}

void MusicPlayer::PlayNote(Sound s)
{
    s.PlaySound(TRACK_OVERTONE);
}

void MusicPlayer::SetBPM(int bpm)
{
    m_fSecondsPerBeat = 60.0 / (float) bpm;
}

void MusicPlayer::SetNoteBuffer(Melody melody)
{
    m_NoteBuffer.m_Notes.clear();
    m_NoteBuffer = melody;
    m_nNoteBufferIndex = 0;
}

void MusicPlayer::SetRhythmBuffer(vector<int> buffer, int size)
{
    m_RhythmBuffer.clear();
    m_RhythmBuffer = buffer;
    m_nNumBeats = size;
    m_nRhythmBufferIndex = 0;
    Static::PrintVector(m_RhythmBuffer);
}

void MusicPlayer::SetInjectedMelody(Melody melody)
{
    m_InjectedNoteBuffer = melody;
    m_nInjectedNoteBufferIndex = 0;
}

void MusicPlayer::SetInjectedRhythm(vector<int> rhythm, int size)
{
    m_InjectedRhythmBuffer.clear();
    m_InjectedRhythmBuffer = rhythm;
    m_nInjectedNumBeats = size;
    m_nInjectedRhythmBufferIndex = 0;
    m_fTimeStepBuffer = m_fSecondsPerBeat;
}
