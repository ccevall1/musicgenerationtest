#ifndef PLAYER_H
#define PLAYER_H

#include "GameObject.h"

using namespace std;

class Player : public GameObject
{

    public:
        Player(SDL_Renderer* gRenderer, Sprite* sprite, MusicEventHandler* musicEventHandler, vector<float> pos, vector<float> rot, float maxSpeed, float accelRate);
        ~Player();
        void Update(float deltaTime);
        void UpdatePhysics(float deltaTime);
        void Draw();
        void HandleEvent(SDL_Event e);
        void HandleCollision(Collider* coll);
        void DrawCollider();
        void SetIsFalling(bool bFalling) { m_bIsFalling = bFalling; }


    private:
        float m_fAccelerationRate; //in reference to the horizontal acceleration (vertical is gravity)
        vector<float> m_vCurrVelocity;
        float m_fMaxSpeed;

        bool m_bIsAcceleratingRight = false;
        bool m_bIsAcceleratingLeft = false;

        bool m_bIsFalling = false; 
        bool m_bIsJumping = false; 
        float m_fJumpTimer;
        bool m_bHasStartedDescendMelody = false;

};

#endif
