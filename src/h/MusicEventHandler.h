#ifndef MUSIC_EVENT_HANDLER_H
#define MUSIC_EVENT_HANDLER_H

#include "MusicGenerator.h"
#include "MusicPlayer.h"

class MusicEventHandler
{
    public:
        MusicEventHandler(MusicGenerator* musicGenerator, MusicPlayer* musicPlayer);
        void NotifyPlayerSpeedChange(float fHorSpeedChange, float fVertSpeedChange, float fHorSpeed, float fVertSpeed);
        void InjectMelody(MusicGenerator::MelodyType eMelodyType, MusicGenerator::RhythmType eRhythmType, int nSpeedChange);
        void AddHigherNote(float deltaTime);
        void AddLowerNote(float deltaTime);
        void SetNote(int note);
        void PlayCurrentNote();

    private:
        MusicGenerator* m_MusicGenerator;
        MusicPlayer* m_MusicPlayer;

        int m_nCurrNote;
        float m_fTimeSinceLastNote;

};

#endif
