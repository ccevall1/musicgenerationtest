#ifndef TILE_H
#define TILE_H

#include "GameObject.h"

class Tile : public GameObject
{

    public:
        Tile(SDL_Renderer* gRenderer, Sprite* sprite, std::vector<float> pos, std::vector<float> rot);
        ~Tile();
        void Update(float deltaTime);
        void Draw();
        void HandleEvent(SDL_Event e) { return; }
        void DrawCollider();

    private:

};

#endif
