#ifndef COLLIDER_H
#define COLLIDER_H

#include <vector>

using namespace std;

class GameObject;

typedef enum {Circle, Rectangle} ColliderType;

class Collider
{

    public:

        Collider(GameObject* parent, int radius);
        Collider(GameObject* parent, int width, int height);

        bool CheckCollision(Collider* coll);
        ColliderType GetType() { return m_eType; }
        GameObject* GetParent() { return m_Parent; }
        int GetRadius() { return m_nRadius; }
        int GetWidth() { return m_nWidth; }
        int GetHeight() { return m_nHeight; }
        bool GetIsStatic() { return m_bIsStatic; }
        void SetIsStatic(bool bIsStatic) { m_bIsStatic = bIsStatic; }

    private:
        ColliderType m_eType;
        int m_nRadius, m_nWidth, m_nHeight;
        GameObject* m_Parent;
        bool m_bIsStatic = false;
};

#endif
