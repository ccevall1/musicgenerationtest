#ifndef MUSIC_GENERATOR_H
#define MUSIC_GENERATOR_H

#include <stdlib.h>
#include <deque>

#include "EuclideanRhythm.h"
#include "Sound.h"
#include "Static.h"
#include "Constants.h"

using namespace Constants;

class MusicGenerator
{

    public:
        enum RomanNumeral {I, II, III, IV, V, VI, VII, END};
        enum MelodyType {DefaultMelody, Ascending, Descending};
        enum RhythmType {DefaultRhythm, SingleBeat, Short, Medium, Long};

        MusicGenerator(vector<Sound> noteBuffer);
        void Cleanup();
        void GenerateMusic();
        Chord CreateChord(MusicGenerator::RomanNumeral eChord);
        Chord CreateChordFromNote(int nBaseNoteIndex);
        MusicGenerator::RomanNumeral NextMajorChordProgression(MusicGenerator::RomanNumeral eChord);
        EuclideanRhythm* GetRhythm() { return m_EuclideanRhythm; }
        Melody GetDefaultMelody() { return m_Melody; }
        EuclideanRhythm* GenerateInjectedRhythm(MusicGenerator::RhythmType eType);
        Melody GenerateInjectedMelody(MusicGenerator::MelodyType eType);

        Sound GetNoteByIndex(int idx) { return m_NotePool[idx]; }

    private:
        vector<Sound> m_NotePool;
        EuclideanRhythm* m_EuclideanRhythm;
        Melody m_Melody; //default melody
        Melody m_StoredInjectedMelody;
        EuclideanRhythm* m_StoredInjectedRhythm;

};

#endif
