#ifndef GAME_OBJECT_H
#define GAME_OBJECT_H

#include <SDL.h>
#include <vector>
#include <algorithm>

#include "Constants.h"
#include "MusicEventHandler.h"

class Collider;

class GameObject
{

    public:
        GameObject(SDL_Renderer* gRenderer, Sprite* sprite, MusicEventHandler* musicEventHandler, std::vector<float> pos, std::vector<float> rot)
        : m_gRenderer(NULL), m_Sprite(NULL), m_MusicEventHandler(NULL), m_vPosition(pos), m_vOrientation(rot)
        {
            if (sprite)
                m_Sprite = sprite;
            if (gRenderer)
                m_gRenderer = gRenderer;
            if (musicEventHandler)
                m_MusicEventHandler = musicEventHandler;
        }
        virtual void Update(float deltaTime) = 0;
        virtual void Draw() = 0;
        virtual void HandleEvent(SDL_Event e) = 0;
        std::vector<float> GetPosition() { return m_vPosition; }
        std::vector<float> GetRotation() { return m_vOrientation; }
        Collider* GetCollider() { return m_Collider; }

    protected:
        SDL_Renderer* m_gRenderer;
        Sprite* m_Sprite;
        std::vector<float> m_vPosition;
        std::vector<float> m_vOrientation;
        Collider* m_Collider;

        MusicEventHandler* m_MusicEventHandler;

};

#endif
