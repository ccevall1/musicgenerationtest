// Class to handle playing melodies given note buffer, bpm, and timestep
// Will have a reference to the music generator class so it can request new notes

#ifndef MUSIC_PLAYER_H
#define MUSIC_PLAYER_H

#include "Sound.h"
#include <vector>
#include "Constants.h"

using namespace std;
using namespace Constants;

class MusicPlayer
{

    public:
        MusicPlayer();
        ~MusicPlayer();
        void Run(float deltaTime);
        void PlayNote(Sound s);
        void SetNoteBuffer(Melody melody);
        void SetRhythmBuffer(vector<int> buffer, int size);
        void SetInjectedMelody(Melody melody);
        void SetInjectedRhythm(vector<int> rhythm, int size);

        void SetBPM(int bpm);
        void LockBPM() { m_bBPMLockEnabled = true; }
        void ReleaseBPMLock() { m_bBPMLockEnabled = false; }
        bool IsBPMLocked() { return m_bBPMLockEnabled; }

        void SetBassNote(Sound* s) { m_BassNote = s; }

    private:
        Melody m_NoteBuffer;
        vector<int> m_RhythmBuffer;
        float m_fSecondsPerBeat;  // easier to handle logically, computed from BPM
        float m_fTimeStepBuffer;  // timer for how many seconds have passed since last time step
        int   m_nNoteBufferIndex;
        int   m_nRhythmBufferIndex;
        int   m_nNumBeats;

        Melody m_InjectedNoteBuffer;
        vector<int> m_InjectedRhythmBuffer;
        int   m_nInjectedNoteBufferIndex;
        int   m_nInjectedRhythmBufferIndex;
        int   m_nInjectedNumBeats;

        Sound* m_BassNote;

        bool  m_bBPMLockEnabled;
};

#endif
