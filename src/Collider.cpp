#include "GameObject.h"
#include "Collider.h"

Collider::Collider(GameObject* parent, int radius)
: m_nWidth(-1),
  m_nHeight(-1),
  m_Parent(parent)
{
    m_eType = ColliderType::Circle;
    m_nRadius = radius;
}

Collider::Collider(GameObject* parent, int width, int height)
: m_nRadius(-1),
  m_Parent(parent)
{
    m_eType = ColliderType::Rectangle;
    m_nWidth = width;
    m_nHeight = height;
}

bool Collider::CheckCollision(Collider* coll)
{
    if (m_eType == ColliderType::Circle)
    {
        if (coll->GetType() == ColliderType::Circle)
        {
            float dist = sqrt(pow(m_Parent->GetPosition()[0] - coll->GetParent()->GetPosition()[0],2) 
                                + pow(m_Parent->GetPosition()[1] - coll->GetParent()->GetPosition()[1],2));
            if (dist < max(m_nRadius,coll->GetRadius()))
            {
                return true;
            }
        }
        else if (coll->GetType() == ColliderType::Rectangle)
        {
            //TODO: Circle to rectangle collision
        }
    }
    else if (m_eType == ColliderType::Rectangle)
    {
        vector<float> corners;
        corners.push_back(m_Parent->GetPosition()[0]); // left x
        corners.push_back(m_Parent->GetPosition()[0] + m_nWidth);  // right x
        corners.push_back(m_Parent->GetPosition()[1]);   // top y
        corners.push_back(m_Parent->GetPosition()[1] + m_nHeight);   // bottom y

        if (coll->GetType() == ColliderType::Circle)
        {
            //TODO: Circle to rectangle collision
        }
        else if (coll->GetType() == ColliderType::Rectangle)
        {
            vector<float> coll_corners;
            GameObject* parent = coll->GetParent();
            coll_corners.push_back(parent->GetPosition()[0]);
            coll_corners.push_back(parent->GetPosition()[0] + coll->GetWidth());
            coll_corners.push_back(parent->GetPosition()[1]);
            coll_corners.push_back(parent->GetPosition()[1] + coll->GetHeight());
            if (corners[0] >= coll_corners[1]) return false;
            if (corners[1] <= coll_corners[0]) return false;
            if (corners[2] >= coll_corners[3]) return false;
            if (corners[3] <= coll_corners[2]) return false;
            return true;
        }
    }
    return false;
}
