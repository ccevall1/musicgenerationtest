#include "MusicEventHandler.h"

MusicEventHandler::MusicEventHandler(MusicGenerator* musicGenerator, MusicPlayer* musicPlayer)
: m_MusicGenerator(musicGenerator),
  m_MusicPlayer(musicPlayer)
{}

void MusicEventHandler::NotifyPlayerSpeedChange(float fHorSpeedChange, float fVertSpeedChange, float fHorSpeed, float fVertSpeed)
{
    if (fHorSpeed < 0) fHorSpeed = -1*fHorSpeed;
    int newBPM = (int) ((0.3*((fHorSpeed*Constants::DEFAULT_BPM) / (float)Constants::PLAYER_SPEED)) + Constants::DEFAULT_BPM);
    if (!m_MusicPlayer->IsBPMLocked())
        m_MusicPlayer->SetBPM(newBPM);
}

void MusicEventHandler::InjectMelody(MusicGenerator::MelodyType eMelodyType, MusicGenerator::RhythmType eRhythmType, int nSpeedChange)
{
    Melody injectedMelody = m_MusicGenerator->GenerateInjectedMelody(eMelodyType);
    EuclideanRhythm* injectedRhythm = m_MusicGenerator->GenerateInjectedRhythm(eRhythmType);
    if (nSpeedChange > 0)
    {
        m_MusicPlayer->SetBPM(nSpeedChange*Constants::DEFAULT_BPM);
        m_MusicPlayer->LockBPM();
    }
    m_MusicPlayer->SetInjectedMelody(injectedMelody);
    m_MusicPlayer->SetInjectedRhythm(injectedRhythm->GetRhythm(), injectedRhythm->GetLength());
}

void MusicEventHandler::AddHigherNote(float deltaTime)
{
    m_fTimeSinceLastNote += deltaTime;
    if (m_fTimeSinceLastNote > Constants::MIN_NOTE_SPACING)
    {
        m_nCurrNote = min(m_nCurrNote + 2, Constants::NUM_NOTES_OCTAVE-1);
        if (m_nCurrNote < Constants::NUM_NOTES_OCTAVE-1)
            PlayCurrentNote();
    }
}
void MusicEventHandler::AddLowerNote(float deltaTime)
{
    m_fTimeSinceLastNote += deltaTime;
    if (m_fTimeSinceLastNote > Constants::MIN_NOTE_SPACING)
    {
        m_nCurrNote = max(-1, m_nCurrNote - 2);
        if (m_nCurrNote > 0)
            PlayCurrentNote();
    }
}
void MusicEventHandler::SetNote(int note)
{
    m_nCurrNote = note;
}
void MusicEventHandler::PlayCurrentNote()
{
/*    Melody mel;
    mel.m_nLength = 1;
    mel.m_Notes.push_back(m_MusicGenerator->CreateChordFromNote(m_nCurrNote));
    m_MusicPlayer->SetInjectedMelody(mel);
    m_MusicPlayer->SetInjectedRhythm(m_MusicGenerator->GenerateInjectedRhythm(MusicGenerator::RhythmType::SingleBeat)->GetRhythm(),1);*/
    cout << "m_nCurrNote: " << m_nCurrNote << endl;
    m_fTimeSinceLastNote = 0;
    m_MusicPlayer->PlayNote(m_MusicGenerator->GetNoteByIndex(Constants::A_MAJOR[m_nCurrNote]));
}


