#include "MusicGenerator.h"
#include "ResourceManager.h"

MusicGenerator::MusicGenerator(vector<Sound> notePool)
: m_EuclideanRhythm(NULL)
{
    m_NotePool = notePool;
}

void MusicGenerator::GenerateMusic()
{
    if (m_EuclideanRhythm)
        delete m_EuclideanRhythm;
    m_Melody.m_Notes.clear();

    // Generate rhythm:
    int secondNum = pow(2, (rand() % 2) + 3); // 8 or 16
    int firstNum = 2*((rand() % 3)+1) + 1;
    cout << firstNum << "," << secondNum << endl;
    m_EuclideanRhythm = new EuclideanRhythm(firstNum,secondNum);

    // Generate chords:
    // first chord generated is random
    //RomanNumeral eChord = RomanNumeral(rand() % RomanNumeral::END);
    RomanNumeral eChord = RomanNumeral::I;
    m_Melody.m_Notes.push_back(CreateChord(eChord));

    // remaining are based on state machine
    for (int i = 0; i < secondNum-firstNum-1; i++)
    {
        eChord = NextMajorChordProgression(eChord);
        m_Melody.m_Notes.push_back(CreateChord(eChord));
    }

    m_Melody.m_nLength = secondNum - firstNum;
}

EuclideanRhythm* MusicGenerator::GenerateInjectedRhythm(RhythmType eType)
{
    switch(eType)
    {
        case RhythmType::DefaultRhythm:
            return m_EuclideanRhythm;
        case RhythmType::SingleBeat:
            m_StoredInjectedRhythm = new EuclideanRhythm(0,1);
            break;
        case RhythmType::Short:
            m_StoredInjectedRhythm = new EuclideanRhythm(0,4);
            break;
        case RhythmType::Medium:
            m_StoredInjectedRhythm = new EuclideanRhythm(0,8);
            break;
        case RhythmType::Long:
            m_StoredInjectedRhythm = new EuclideanRhythm(0,16);
            break;
        default:
            m_StoredInjectedRhythm = new EuclideanRhythm(0,1);
            break;
    }

    return m_StoredInjectedRhythm;
}

Melody MusicGenerator::GenerateInjectedMelody(MelodyType eType)
{
    //m_StoredInjectedMelody = new Melody;
    switch(eType)
    {
        case MelodyType::DefaultMelody:
            return m_Melody; // returns generic melody
        case MelodyType::Ascending:
            m_StoredInjectedMelody.m_Notes.push_back(CreateChordFromNote(0));
            m_StoredInjectedMelody.m_Notes.push_back(CreateChordFromNote(2));
            m_StoredInjectedMelody.m_Notes.push_back(CreateChordFromNote(4));
            m_StoredInjectedMelody.m_Notes.push_back(CreateChordFromNote(2));
            m_StoredInjectedMelody.m_nLength = 4;
            break;
        case MelodyType::Descending:
            m_StoredInjectedMelody.m_Notes.push_back(CreateChordFromNote(4));
            m_StoredInjectedMelody.m_Notes.push_back(CreateChordFromNote(2));
            m_StoredInjectedMelody.m_Notes.push_back(CreateChordFromNote(1));
            m_StoredInjectedMelody.m_Notes.push_back(CreateChordFromNote(0));
            m_StoredInjectedMelody.m_nLength = 3;
            break;
        default:
            break;
    }
    return m_StoredInjectedMelody;
}

void MusicGenerator::Cleanup()
{
    delete m_EuclideanRhythm;
    m_Melody.m_Notes.clear();
}

Chord MusicGenerator::CreateChord(RomanNumeral eChord)
{
    // For now let's just do C major because other keys are hard
    Chord chord;
    //Choose three random notes from the chord to play to vary it up (chance for duplicates == fewer played)
    chord.push_back(m_NotePool[Constants::A_Major_RomanNumeralVectors[eChord][rand()%3]]);
    chord.push_back(m_NotePool[Constants::A_Major_RomanNumeralVectors[eChord][rand()%3]]);
    chord.push_back(m_NotePool[Constants::A_Major_RomanNumeralVectors[eChord][rand()%3]]);

    return chord;
}

Chord MusicGenerator::CreateChordFromNote(int nBaseNoteIndex)
{
    Chord chord;
    chord.push_back(m_NotePool[Constants::A_MAJOR[nBaseNoteIndex]]);
    return chord;
}

MusicGenerator::RomanNumeral MusicGenerator::NextMajorChordProgression(RomanNumeral eChord)
{

    RomanNumeral ret;
    int result;
    switch(eChord)
    {
        case RomanNumeral::I:
            ret = RomanNumeral((rand() % RomanNumeral::END-1) + 1);
            break;
        case RomanNumeral::II:
            result = rand() % 2;
            ret = (result == 0 ? RomanNumeral::VII : RomanNumeral::V);
            break;
        case RomanNumeral::III:
            result = rand() % 2;
            ret = (result == 0 ? RomanNumeral::VI : RomanNumeral::IV);
            break;
        case RomanNumeral::IV:
            result = rand() % 4;
            if (result == 0) ret = RomanNumeral::II;
            else if (result == 1) ret = RomanNumeral::V;
            else if (result == 2) ret = RomanNumeral::VII;
            else if (result == 3) ret = RomanNumeral::I;
            break;
        case RomanNumeral::V:
            result = rand() % 2;
            ret = (result == 0 ? RomanNumeral::I : RomanNumeral::VI);
            break;
        case RomanNumeral::VI:
            result = rand() % 3;
            if (result == 0) ret = RomanNumeral::II;
            else if (result == 1) ret = RomanNumeral::IV;
            else if (result == 2) ret = RomanNumeral::V;
            break;
        case RomanNumeral::VII:
            result = rand() % 2;
            ret = (result == 0 ? RomanNumeral::I : RomanNumeral::V);
            break;
        default:
            ret = RomanNumeral::I;
            break;
    }
    return ret;

}
