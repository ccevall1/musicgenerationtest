#include "Collider.h"
#include "Player.h"

Player::Player(SDL_Renderer* gRenderer, Sprite* sprite, MusicEventHandler* musicEventHandler, vector<float> pos, vector<float> rot, float maxSpeed, float accelRate)
: GameObject(gRenderer, sprite, musicEventHandler, pos, rot)
{
    m_fAccelerationRate = accelRate;
    m_fMaxSpeed = maxSpeed;
    m_vCurrVelocity.push_back(0.0);
    m_vCurrVelocity.push_back(0.0);

    m_Collider = new Collider(this, m_Sprite->width, m_Sprite->height);

    m_MusicEventHandler->SetNote(0);
}

Player::~Player()
{
    delete m_Collider;
}

void Player::Update(float deltaTime)
{
    vector<float> vCurrVelocity = m_vCurrVelocity;
    UpdatePhysics(deltaTime);
    if (m_vCurrVelocity[0] != vCurrVelocity[0] || m_vCurrVelocity[1] != vCurrVelocity[1])
    {
        float horChange = m_vCurrVelocity[0] - vCurrVelocity[0];
        float verChange = m_vCurrVelocity[1] - vCurrVelocity[1];
        m_MusicEventHandler->NotifyPlayerSpeedChange(horChange, verChange, m_vCurrVelocity[0], m_vCurrVelocity[1]);
    }
}

void Player::UpdatePhysics(float deltaTime)
{
    m_vPosition[0] += m_vCurrVelocity[0]*deltaTime;
    m_vPosition[1] += m_vCurrVelocity[1]*deltaTime;

    // Block movement past boundaries
    if (m_vPosition[0] < 0)
    {
        m_vPosition[0] = 0;
        m_vCurrVelocity[0] = 0;
    }
    else if (m_vPosition[0] > Constants::SCREEN_WIDTH - m_Sprite->width)
    {
        m_vPosition[0] = Constants::SCREEN_WIDTH - m_Sprite->width;
        m_vCurrVelocity[0] = 0;
    }

    // Move left or right
    if (m_bIsAcceleratingRight)
    {
        m_vCurrVelocity[0] += m_fAccelerationRate*deltaTime;
        if (m_vCurrVelocity[0] > m_fMaxSpeed) m_vCurrVelocity[0] = m_fMaxSpeed;
    }
    else if (m_bIsAcceleratingLeft)
    {
        m_vCurrVelocity[0] -= m_fAccelerationRate*deltaTime;
        if (m_vCurrVelocity[0] < -1*m_fMaxSpeed) m_vCurrVelocity[0] = -1*m_fMaxSpeed;
    }
    else //deccelerate
    {
        if (m_vCurrVelocity[0] < 0)
        {
            m_vCurrVelocity[0] += m_fAccelerationRate*deltaTime;
            if (m_vCurrVelocity[0] > 0) m_vCurrVelocity[0] = 0;
        }
        else
        {
            m_vCurrVelocity[0] -= m_fAccelerationRate*deltaTime;
            if (m_vCurrVelocity[0] < 0) m_vCurrVelocity[0] = 0;
        }
    }

    // Gravity
    if (m_bIsFalling && !m_bIsJumping)
    {
        // fall
        m_vCurrVelocity[1] += Constants::GRAVITY_FORCE*deltaTime;
        m_MusicEventHandler->AddLowerNote(deltaTime);
    }
    else
    {
        m_vCurrVelocity[1] = 0;
    }

    // Jumping
    if (m_fJumpTimer > 0)
        m_fJumpTimer -= deltaTime;
    else
        m_bIsJumping = false;

    const Uint8* currentKeyStates = SDL_GetKeyboardState(NULL);
    if (currentKeyStates[SDL_SCANCODE_UP] || currentKeyStates[SDL_SCANCODE_W])
    {
        if (!m_bIsJumping && !m_bIsFalling)
        {
            m_fJumpTimer = Constants::PLAYER_MAX_JUMP_TIME;
            m_bIsJumping = true;
            //m_bHasStartedDescendMelody = false;
        }

        m_MusicEventHandler->AddHigherNote(deltaTime);
    }

    if (m_bIsJumping)
    {
        m_vCurrVelocity[1] = -1*Constants::PLAYER_JUMP_SPEED;
    }
}

void Player::Draw()
{
    SDL_Rect dimen = {.x = (int)m_vPosition[0], .y = (int)m_vPosition[1], .w = 64, .h = 64};
    SDL_RenderCopy(m_gRenderer, m_Sprite->m_tTexture, NULL, &dimen);
    if (Constants::DEBUG_COLLIDERS)
        DrawCollider();
}

void Player::DrawCollider()
{
    if (m_Collider->GetType() == ColliderType::Rectangle)
    {
        SDL_Rect rect = {m_vPosition[0], m_vPosition[1], m_Collider->GetWidth(), m_Collider->GetHeight()};
        SDL_SetRenderDrawColor(m_gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);
        SDL_RenderFillRect(m_gRenderer, &rect);
    }
}

void Player::HandleEvent(SDL_Event e)
{
    if (e.type == SDL_KEYDOWN)
    {
        switch(e.key.keysym.sym)
        {
            case SDLK_LEFT: case SDLK_a:
                m_bIsAcceleratingRight = false;
                m_bIsAcceleratingLeft = true;
                break;
            case SDLK_RIGHT: case SDLK_d:
                m_bIsAcceleratingRight = true;
                m_bIsAcceleratingLeft = false;
                break;
           default:
                break;
        }
    }
    else if (e.type == SDL_KEYUP)
    {
        switch(e.key.keysym.sym)
        {
            case SDLK_LEFT: case SDLK_a:
                m_bIsAcceleratingLeft = false;
                break;
            case SDLK_RIGHT: case SDLK_d:
                m_bIsAcceleratingRight = false;
                break;
            case SDLK_UP: case SDLK_w:
                m_bIsJumping = false;
                break;
            default:
                break;
        }
    }
}

void Player::HandleCollision(Collider* coll)
{
    if (coll->GetIsStatic())
    {
        if (coll->GetType() == ColliderType::Rectangle)
        {
            //Horizontal movement
            if ((m_vPosition[0] + m_Collider->GetWidth() > coll->GetParent()->GetPosition()[0]) &&
                 m_vPosition[0] < coll->GetParent()->GetPosition()[0] &&
                 m_vPosition[1] + m_Collider->GetHeight() <= coll->GetParent()->GetPosition()[1])
            {
                //cout << "approach from left case" << endl;
                m_vPosition[0] -= 1;
                m_vCurrVelocity[0] = 0;
            }
            else if ((m_vPosition[0] < coll->GetParent()->GetPosition()[0] + coll->GetWidth()) &&
                m_vPosition[0] + m_Collider->GetWidth() > coll->GetParent()->GetPosition()[0] + coll->GetWidth() &&
                m_vPosition[1] + m_Collider->GetHeight() <= coll->GetParent()->GetPosition()[1])
            {
                //cout << "approach from right case" << endl;
                m_vPosition[0] += 1;
                m_vCurrVelocity[0] = 0;
            }
            //Vertical movement
            if (m_vPosition[1] + m_Collider->GetHeight() >= coll->GetParent()->GetPosition()[1])
            {
                //cout << "approach from above case" << endl;
                //m_vPosition[1] = coll->GetParent()->GetPosition()[1] - m_Collider->GetHeight();
                //m_vCurrVelocity[1] = 0;
                m_bIsFalling = false;
                m_bIsJumping = false;
            }
            /*else if (m_vPosition[1] < coll->GetParent()->GetPosition()[0] + coll->GetParent->GetSprite()->GetWidth())
            {
                m_vPosition[0] += 1;
                m_vCurrVelocity[0] = 0;
            }*/  //probably won't be cases of hitting platforms from below, plus you'd want to jump into them anyway
           
        }
        else if (coll->GetType() == ColliderType::Circle)
        {

        }
    }
}
